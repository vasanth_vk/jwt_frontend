let firstname = document.getElementById("firstname");
let lastname = document.getElementById("lastname");
let email = document.getElementById("email");
let password = document.getElementById("password");
let registerSubmit = document.getElementById("register-submit");
let invalidFeedback = document.querySelector(".invalid-feedback");
let validFeedback = document.querySelector(".valid-feedback");

registerSubmit.addEventListener("click", (e) => {
    e.preventDefault();
    let url = "https://intense-caverns-45038.herokuapp.com/register";
    const user = {
        firstName: firstname.value,
        lastName: lastname.value,
        email: email.value,
        password: password.value,
    };
    fetch(url, {
        method: "POST",
        body: JSON.stringify(user),
        headers: { "Content-type": "application/json" },
        mode: "cors",
    })
        .then((res) => res.json())
        .then((data) => {
            if (data.message == "userExists") {
                invalidFeedback.textContent = "User already exists";
            } else {
                validFeedback.textContent =
                    "New account created redirecting to login page";
                let token = data.accessToken;
                console.log(token);
                localStorage.setItem("jwt_access_token", JSON.stringify(token));
                setTimeout(() => {
                    location.assign("./dashboard.html");
                }, 500);
            }
        })
        .catch((err) => {
            console.log(err);
        });
});
