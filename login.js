let email = document.getElementById("email");
let password = document.getElementById("password");
let loginSubmit = document.getElementById("login-submit");
let invalidFeedback = document.querySelector(".invalid-feedback");
let navLogin = document.getElementById("nav-login-btn");
let navSignup = document.getElementById("nav-signup-btn");

window.onload = () => {
    let token = localStorage.getItem("jwt_access_token");
    if (token != undefined && token) {
        let url =
            "https://intense-caverns-45038.herokuapp.com/login/authenticate";
        console.log(token);
        let header = new Headers();
        header.append("authorization", `Bearer ${JSON.parse(token)}`);
        header.append("Content-type", "application/json");
        fetch(url, {
            method: "GET",
            headers: header,
            mode: "cors",
        })
            .then((res) => res.json())
            .then((data) => {
                console.log(data);
                if (data.authenticated) {
                    location.assign("./dashboard.html");
                }
            })
            .catch((err) => {
                console.log(err);
            });
    }
};

navLogin.addEventListener("click", (e) => {
    e.preventDefault();
    let token = localStorage.getItem("jwt_access_token");
    if (token) {
        console.log(token);
        let url =
            "https://intense-caverns-45038.herokuapp.com/login/authenticate";
        let header = new Headers();
        header.append("authorization", `Bearer ${JSON.parse(token)}`);
        header.append("Content-type", "application/json");
        fetch(url, {
            method: "GET",
            headers: header,
            mode: "cors",
        })
            .then((res) => res.json())
            .then((data) => {
                console.log(data);
            })
            .catch((err) => {
                console.log(err);
            });
        location.assign("./dashboard.html");
    }
});

loginSubmit.addEventListener("click", (e) => {
    invalidFeedback.textContent = "";

    e.preventDefault();
    let url = "https://intense-caverns-45038.herokuapp.com/login";
    const user = {
        email: email.value,
        password: password.value,
    };
    if (localStorage.getItem("jwt_access_token")) {
        location.assign("./dashboard.html");
    } else {
        fetch(url, {
            method: "POST",
            body: JSON.stringify(user),
            headers: { "Content-type": "application/json" },
            mode: "cors",
        })
            .then((res) => res.json())
            .then((data) => {
                console.log(data);
                if (data.accessToken) {
                    localStorage.setItem(
                        "jwt_access_token",
                        JSON.stringify(data.accessToken)
                    );
                    console.log(data.accessToken);
                    location.assign("./dashboard.html");
                } else {
                    invalidFeedback.textContent = data.message;
                }
            })
            .catch((err) => {
                console.log(err);
            });
    }
});
