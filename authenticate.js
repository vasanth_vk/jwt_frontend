let userName = document.getElementById("user-name");

window.onload = () => {
    let token = localStorage.getItem("jwt_access_token");
    if (token) {
        let url =
            "https://intense-caverns-45038.herokuapp.com/login/authenticate";
        let header = new Headers();
        header.append("authorization", `Bearer ${JSON.parse(token)}`);
        header.append("Content-type", "application/json");
        fetch(url, {
            method: "GET",
            headers: header,
            mode: "cors",
        })
            .then((res) => res.json())
            .then((data) => {
                console.log(data);
                if (!data.authenticated) {
                    location.assign("./login.html");
                } else {
                    userName.textContent = `DASHBOARD ${data.user}`;
                }
            })
            .catch((err) => {
                console.log(err);
            });
    } else {
        location.assign("./login.html");
    }
};
