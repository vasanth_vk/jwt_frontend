const logout = document.querySelector("#logout");
logout.style.cursor = "pointer";
logout.addEventListener("click", () => {
    localStorage.setItem("jwt_access_token", "");
    location.assign("./login.html");
});
